package com.parkin.swaggerjwt.auth;

import javax.validation.Valid;

import com.parkin.swaggerjwt.config.security.JwtTokenUtil;
import com.parkin.swaggerjwt.model.User;
import com.parkin.swaggerjwt.model.dto.AuthRequest;
import com.parkin.swaggerjwt.repositorys.CustomerRepository;

import io.swagger.annotations.Api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping("/auth")
@Api( tags = "Auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    
    @Autowired
    private CustomerRepository repository;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@Valid @RequestBody AuthRequest request) {
        try {
            System.out.println("equest.getUsername(): "+ request.getUsername());
            Authentication authenticate = authenticationManager
                .authenticate(
                    new UsernamePasswordAuthenticationToken(
                        request.getUsername(), request.getPassword()
                    )
                );
            User user = (User) authenticate.getPrincipal();

            String token = jwtTokenUtil.generateToken(user);

            return ResponseEntity.ok()
                .header(
                    HttpHeaders.AUTHORIZATION,
                    // jwtTokenUtil.generateAccessToken(user)
                    // jwtTokenUtil.generateToken(user)
                    token
                )
                .body(token);
        } catch (BadCredentialsException ex) {
            
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

}
