package com.parkin.swaggerjwt;

import com.parkin.swaggerjwt.model.Book;
import com.parkin.swaggerjwt.model.Customer;
import com.parkin.swaggerjwt.model.User;
import com.parkin.swaggerjwt.model.UserAuth;
import com.parkin.swaggerjwt.repositorys.BookRepo;
import com.parkin.swaggerjwt.repositorys.CustomerRepository;
import com.parkin.swaggerjwt.repositorys.UserAuthRepository;
import com.parkin.swaggerjwt.repositorys.UserRepo;
import com.parkin.swaggerjwt.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@EnableScheduling
// @MapperScan(value = "com.ruifeng.demo.dao")
public class SpringBootSwaggerUIApplication implements CommandLineRunner {

    @Value("${context-path}")
    private String contextPath;

    @Autowired
    private CustomerRepository repository;

    @Autowired
    private UserAuthRepository userAuthRepository;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private UserService userService;

    @Autowired
    BookRepo bookRepo;

    public static void main(final String[] args) {
        // System.setProperty("server.servlet.context-path", "/api");
        SpringApplication.run(SpringBootSwaggerUIApplication.class, args);
    }

    @Bean
    public WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> webServerFactoryCustomizer() {
        return factory -> factory.setContextPath(contextPath);
    }

    @Override
    public void run(final String... args) throws Exception {



      // inti data
      // repository.deleteAll();
      // userRepo.deleteAll();
      // userAuthRepository.deleteAll();

      // userService.delete("xxx");
  
      // // save a couple of customers
      // repository.save(new Customer("Alice", "Smith"));
      // repository.save(new Customer("Bob", "Smith"));

      // BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
      // userRepo.save(new User("xxx", bCryptPasswordEncoder.encode("ppp")));
      // userAuthRepository.save(new UserAuth("xxx",  bCryptPasswordEncoder.encode("ppp") ));



  
    //   fetch all customers
      // System.out.println("Customers found with findAll():");
      // System.out.println("-------------------------------");
      // for (final Customer customer : repository.findAll()) {
      //   System.out.println(customer);
      //     System.out.println();
      // }
      
      // System.out.println("userRepo found with findAll():");
      // System.out.println("-------------------------------");
      // for (final User user : userRepo.findAll()) {
      //   System.out.println(user);
      // }     

      // System.out.println("userAuthRepository found with findAll():");
      // System.out.println("-------------------------------");
      // for (final UserAuth userAuth : userAuthRepository.findAll()) {
      //   System.out.println(userAuth);
      // }
      
      // bookRepo.deleteAll();
      // System.out.println("bookRepo found with insert():");
      // System.out.println("-------------------------------");
      // for (int i = 0; i < 10; i++) {
      //   Book book = new Book();
      //   book.setId(i);
      //   book.setAuthor("Author" + i);
      //   book.setName("Name" + i);
      //   book.setPrice(new Float((i+1) *  Math.random()) * 36);
      //   book.setIs_recommended(i%2 == 0);
      //   bookRepo.save(book);
      // }


    //   // fetch an individual customer
    //   System.out.println("Customer found with findByFirstName('Alice'):");
    //   System.out.println("--------------------------------");
    //   System.out.println(repository.findByFirstName("Alice"));
  
    //   System.out.println("Customers found with findByLastName('Smith'):");
    //   System.out.println("--------------------------------");
    //   for (Customer customer : repository.findByLastName("Smith")) {
    //     System.out.println(customer);
    //   }
  
    }

}
