package com.parkin.swaggerjwt.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Value("${context-path}")
    private String contextPath;

    private Info info() {
        return new Info()
        .title("My Book Store REST API")
        .description("Rest Api for bookstore web application (SpringBooot:MongoDB)")
        .version("1.0")
        // .termsOfService("Terms of service")
        // .license(new License().name("License of API").url("API license URL"))
        .contact(new Contact().name("Parkin Promsri").email("parkin@ipassion.com").url("https://medium.com/@910pk"));
    }
      
    @Bean
    public GroupedOpenApi apiGroup() {
        return GroupedOpenApi
                .builder()
                .group("Api")
                .pathsToMatch("/**")
                .pathsToExclude("/api-resource-controller/**", "/swagger-resources/**", "/v2/**", "/v3/**")
                .build();
    }

    @Bean
    public OpenAPI apiInfo() {
        final String securitySchemeName = "bearerAuth";
        return new OpenAPI()
            .addSecurityItem(new SecurityRequirement().addList(securitySchemeName))
            .components(
                new Components()
                    .addSecuritySchemes(securitySchemeName,
                        new SecurityScheme()
                            .name(securitySchemeName)
                            .type(SecurityScheme.Type.HTTP)
                            .scheme("bearer")
                            .bearerFormat("JWT")
                    )
            )
            .info(info());
    }

  
}
