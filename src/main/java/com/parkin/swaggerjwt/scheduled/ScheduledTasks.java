package com.parkin.swaggerjwt.scheduled;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import com.parkin.swaggerjwt.services.BookService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {
    static Logger log = LogManager.getLogger(ScheduledTasks.class.getName());

    @Autowired
    private BookService bookService;

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    @Scheduled(cron = "0 1 0 ? * MON")
    public void scheduleTaskWithCronExpression() {
        log.info("bookService Task :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()));
        this.bookService.updateBookData();
    }

}