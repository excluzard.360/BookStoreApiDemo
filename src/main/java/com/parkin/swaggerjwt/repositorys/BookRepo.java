package com.parkin.swaggerjwt.repositorys;

import com.parkin.swaggerjwt.model.exception.NotFoundException;
import com.parkin.swaggerjwt.model.Book;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface BookRepo extends MongoRepository<Book, Long> {

    default Book getById(Long id) {
        return findById(id).orElseThrow(() -> new NotFoundException(Book.class, id));
    }

}
