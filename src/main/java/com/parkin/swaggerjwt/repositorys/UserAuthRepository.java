package com.parkin.swaggerjwt.repositorys;

import com.parkin.swaggerjwt.model.Customer;
import com.parkin.swaggerjwt.model.UserAuth;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserAuthRepository extends MongoRepository<UserAuth, String> {

  public Customer findByUserName(String firstName);

}