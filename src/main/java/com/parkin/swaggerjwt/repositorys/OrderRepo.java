package com.parkin.swaggerjwt.repositorys;

import com.parkin.swaggerjwt.model.exception.NotFoundException;
import com.parkin.swaggerjwt.model.Order;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface OrderRepo extends MongoRepository<Order, ObjectId> {

    default Order getById(ObjectId id) {
        return findById(id).orElseThrow(() -> new NotFoundException(Order.class, id));
    }

}
