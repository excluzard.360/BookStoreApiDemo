package com.parkin.swaggerjwt.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.parkin.swaggerjwt.model.Book;
import com.parkin.swaggerjwt.model.dto.BookList;
import com.parkin.swaggerjwt.model.dto.BookResponse;
import com.parkin.swaggerjwt.repositorys.BookRepo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/books")
@Api(tags = "Books")
public class BookRestController {

    @Autowired
    BookRepo bookRepo;

    @ApiOperation(value = "This method is used to get the Book.")
    @GetMapping
    public ResponseEntity<?> getUsre() {
        return ResponseEntity.ok().body(BookResponse.builder().books(bookRepo.findAll()).build());
    }

    // @RequestMapping(value = "/test", method = RequestMethod.POST)
    public ResponseEntity<?> updateBookData() {
        
        RestTemplate restTemplate = new RestTemplate();

        List<Book> books = new ArrayList<>();

        String fooResourceUrl = "https://scb-test-book-publisher.herokuapp.com/books";

        Book[] response = restTemplate.getForObject(fooResourceUrl, Book[].class);

        System.out.println("response : " + response.toString());

        Collections.addAll(books, response);
        bookRepo.saveAll(books);

        // return ResponseEntity.ok().body(BookResponse.builder().books(bookRepo.findAll()).build());
        return ResponseEntity.ok().body(response);
    }

}
