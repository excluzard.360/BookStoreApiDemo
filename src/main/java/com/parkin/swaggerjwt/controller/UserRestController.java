package com.parkin.swaggerjwt.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.parkin.swaggerjwt.config.security.JwtTokenUtil;
import com.parkin.swaggerjwt.model.dto.OrdersRequest;
import com.parkin.swaggerjwt.model.dto.OrdersResponse;
import com.parkin.swaggerjwt.model.dto.UserRequest;
import com.parkin.swaggerjwt.repositorys.UserRepo;
import com.parkin.swaggerjwt.services.OrderService;
import com.parkin.swaggerjwt.services.UserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

@RestController @RequestMapping("/users")
@Api( tags = "Users")
public class UserRestController {

    @Autowired
    UserService userService;

    @Autowired
    OrderService orderService;

    @Autowired
    UserRepo userRepo;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @ApiOperation(value = "This method is used to get the user.")
    @GetMapping
    public ResponseEntity<?> getUsre(HttpServletRequest request) throws Exception {
        return ResponseEntity.ok().body(userService.fineUserAndBookByUserName(jwtTokenUtil.getUserName(request)));
    }

    @ApiOperation(value = "This method is used to delete the user.")
    @DeleteMapping
    // public ResponseEntity<?>rmUser(@RequestParam String username) throws Exception {
    public ResponseEntity<?>rmUser(HttpServletRequest request) throws Exception {
        userService.delete(jwtTokenUtil.getUserName(request));
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "This method is used to create the user.")
    @PostMapping
    public ResponseEntity<?> createUser(HttpServletResponse response, @Valid @RequestBody UserRequest userRequest) throws Exception{
        userService.create(userRequest);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "This method is used to order books")
    @RequestMapping(value = "/orders", method = RequestMethod.POST)
    public ResponseEntity<?> orders(HttpServletResponse response, HttpServletRequest request, @Valid @RequestBody OrdersRequest ordersRequest) {
        OrdersResponse ordersResponse = OrdersResponse.builder().price(orderService.orderBook(ordersRequest, jwtTokenUtil.getUser(request))).build();
        return ResponseEntity.ok().body(ordersResponse);
    }

}
