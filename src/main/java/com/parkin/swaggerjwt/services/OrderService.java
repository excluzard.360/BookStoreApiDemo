package com.parkin.swaggerjwt.services;

import com.parkin.swaggerjwt.model.dto.OrdersRequest;
import com.parkin.swaggerjwt.model.dto.UserRequest;
import com.parkin.swaggerjwt.model.exception.BusinessException;
import com.parkin.swaggerjwt.model.Book;
import com.parkin.swaggerjwt.model.Order;
import com.parkin.swaggerjwt.model.User;
import com.parkin.swaggerjwt.repositorys.BookRepo;
import com.parkin.swaggerjwt.repositorys.OrderRepo;
import com.parkin.swaggerjwt.repositorys.UserRepo;
import lombok.RequiredArgsConstructor;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import static java.lang.String.format;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private BookRepo bookRepo;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Transactional
    public Float orderBook(OrdersRequest request, User user) {

        user = userRepo.findByUsername(user.getUsername()).get();
        Float price = new Float(0.0);

        List<Order> orders = new ArrayList<Order>();
        for (Long bookId : request.getOrders()) {
            Optional<Book> book = bookRepo.findById(bookId);
            if (book.isPresent()) {
                // System.out.println(book.get().getPrice());
                price += book.get().getPrice();
            }
            orders.add(Order.builder().bookId(bookId).userId(user.getId()).build());

        }

        orderRepo.saveAll(orders);

        return price;
    }

    @Transactional
    public User create(UserRequest request) {

        User user = new User();
        user.setDate_of_birth(request.getDate_of_birth());
        user.setUsername(request.getUsername());
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        user.setPassword(bCryptPasswordEncoder.encode(request.getPassword()));
        user = userRepo.save(user);
        return user;

    }

    @Transactional
    public void delete(String username) throws Exception {
        Optional<User> user = userRepo.findByUsername(username);
        if (user.isPresent()) {
            userRepo.deleteById(user.get().getId());
        } else {
            throw new BusinessException("username : " + username + " is not found.");
        }
    }
    
    public List<Order> findAllByUserName(ObjectId id) throws Exception {

        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(id));
        List<Order> users = mongoTemplate.find(query, Order.class);
        return users;
    }    

    public void rmAllByUserName(ObjectId id) throws Exception {
        orderRepo.deleteAll(findAllByUserName(id));
    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepo.findByUsername(username).orElseThrow(
            () -> new UsernameNotFoundException(format("User with username - %s, not found", username)));
    }

}
