package com.parkin.swaggerjwt.services;

import com.parkin.swaggerjwt.model.dto.UserRequest;
import com.parkin.swaggerjwt.model.dto.UserResponse;
import com.parkin.swaggerjwt.model.exception.BusinessException;
import com.parkin.swaggerjwt.model.Order;
import com.parkin.swaggerjwt.model.User;
import com.parkin.swaggerjwt.repositorys.OrderRepo;
import com.parkin.swaggerjwt.repositorys.UserRepo;
import lombok.RequiredArgsConstructor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import static java.lang.String.format;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    static Logger log = LogManager.getLogger(UserService.class.getName());  

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private OrderService orderService;

    @Transactional
    public User create(UserRequest request) {

        User user = new User();
        user.setDate_of_birth(request.getDate_of_birth());
        user.setUsername(request.getUsername());
        user.setName(request.getName());
        user.setSurname(request.getSurname());
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        user.setPassword(bCryptPasswordEncoder.encode(request.getPassword()));
        user = userRepo.save(user);
        return user;

    }

    @Transactional
    public void delete(String username) throws Exception {
        Optional<User> user = userRepo.findByUsername(username);
        if (user.isPresent()) {
            userRepo.deleteById(user.get().getId());
            orderService.rmAllByUserName(user.get().getId());
            log.info("delete -> userId :" + user.get().getId() + " : " + user.get().getUsername() + " -> successfully");
        } else {
            log.error("delete -> username : " + username + " is not found. -> error" );
            throw new BusinessException("username : " + username + " is not found.");
        }
    }

    public List<UserResponse> fineAllUserAndBook() throws Exception {

        List<UserResponse> userResponse = new ArrayList<UserResponse>();
        for (User user : userRepo.findAll()) {

            List<Order> orders = orderService.findAllByUserName(user.getId());
            List<Long> books = orders.stream().map(Order::getBookId).distinct().collect(Collectors.toList());

            userResponse.add(
                UserResponse.builder()
                .username(user.getUsername())
                .name(user.getName())
                .surname(user.getSurname())
                // .password(user.getPassword())
                .date_of_birth(user.getDate_of_birth())
                .books(books)
                .build()
            );

        }
        return userResponse;
    }

    public List<UserResponse> fineUserAndBook(String userName) throws Exception {

        List<UserResponse> userResponse = new ArrayList<UserResponse>();
        for (User user : userRepo.findAll()) {

            List<Order> orders = orderService.findAllByUserName(user.getId());
            List<Long> books = orders.stream().map(Order::getBookId).distinct().collect(Collectors.toList());

            userResponse.add(
                UserResponse.builder()
                .username(user.getUsername())
                .name(user.getName())
                .surname(user.getSurname())
                // .password(user.getPassword())
                .date_of_birth(user.getDate_of_birth())
                .books(books)
                .build()
            );

        }
        return userResponse;
    }

    public List<UserResponse> fineUserAndBookByUserName(String userName) throws Exception {

        List<UserResponse> userResponse = new ArrayList<UserResponse>();
        Optional<User> user = userRepo.findByUsername(userName);

        if(user.isPresent()) {
            List<Order> orders = orderService.findAllByUserName(user.get().getId());
            List<Long> books = orders.stream().map(Order::getBookId).distinct().collect(Collectors.toList());
            userResponse.add(
                UserResponse.builder()
                .username(user.get().getUsername())
                .name(user.get().getName())
                .surname(user.get().getSurname())
                // .password(user.get().getPassword())
                .date_of_birth(user.get().getDate_of_birth())
                .books(books)
                .build()
            );
        }

        return userResponse;
    }

    public List<UserResponse> fineUserAndBook() throws Exception {

        List<UserResponse> userResponse = new ArrayList<UserResponse>();
        for (User user : userRepo.findAll()) {

            List<Order> orders = orderService.findAllByUserName(user.getId());
            List<Long> books = orders.stream().map(Order::getBookId).distinct().collect(Collectors.toList());

            userResponse.add(
                UserResponse.builder()
                .username(user.getUsername())
                .name(user.getName())
                .surname(user.getSurname())
                // .password(user.getPassword())
                .date_of_birth(user.getDate_of_birth())
                .books(books)
                .build()
            );

        }
        return userResponse;
    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepo
                .findByUsername(username)
                .orElseThrow(
                    () -> new UsernameNotFoundException(format("User with username - %s, not found", username))
                );
    }

}
