package com.parkin.swaggerjwt.services;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.parkin.swaggerjwt.model.Book;
import com.parkin.swaggerjwt.repositorys.BookRepo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BookService {

    static Logger log = LogManager.getLogger(BookService.class.getName());
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    @Autowired
    private BookRepo bookRepo;

    @Autowired
    private MongoTemplate mongoTemplate;


    @Transactional
    public List<Book> updateBookData() {
        
        RestTemplate restTemplate = new RestTemplate();

        List<Book> books = new ArrayList<>();

        String resourceUrl = "https://scb-test-book-publisher.herokuapp.com/books";

        Book[] response = restTemplate.getForObject(resourceUrl, Book[].class);

        System.out.println("response : " + response.toString());

        Collections.addAll(books, response);

        

        bookRepo.saveAll(books);

        booksRecommendation();

        log.info("update BookData and booksRecommendation  Task :: Execution Time - {} success.", dateTimeFormatter.format(LocalDateTime.now()));

        return books;

    }

    @Transactional
    public List<Book> booksRecommendation() {
        
        RestTemplate restTemplate = new RestTemplate();

        List<Book> books = new ArrayList<>();

        String resourceUrl = "https://scb-test-book-publisher.herokuapp.com/books/recommendation";

        Book[] response = restTemplate.getForObject(resourceUrl, Book[].class);

        System.out.println("response : " + response.toString());

        for (int i = 0; i < response.length; i++) {
            response[i].setIs_recommended(true);
        }

        Collections.addAll(books, response);
        bookRepo.saveAll(books);

        return books;

    }
}