package com.parkin.swaggerjwt.model;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "books")
public class Book {

    @Id
    private long id;

    @JsonProperty("book_name")
    private String name;
    
    @JsonProperty("author_name")
    private String author;
    
    private Float price;
    
    private Boolean is_recommended;
}

