package com.parkin.swaggerjwt.model.dto;

import lombok.Data;
import lombok.NonNull;

import java.util.Date;

import javax.validation.constraints.NotBlank;

@Data
public class UserRequest {

    // @NotBlank(message = "username is mandatory")
    @NotBlank
    private String username;
    
    // @NotBlank(message = "password is mandatory")
    @NotBlank
    private String password;
    private String  name;
    private String  surname;
    private Date  date_of_birth;
}
