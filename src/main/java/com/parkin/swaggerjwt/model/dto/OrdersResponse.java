package com.parkin.swaggerjwt.model.dto;

import lombok.Builder;
import lombok.Data;

/**
 * OrdersResponse
 */

 @Data
 @Builder
public class OrdersResponse {

    private Float price;
    
}