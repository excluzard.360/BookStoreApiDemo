package com.parkin.swaggerjwt.model.dto;

import java.util.Date;
import java.util.List;

import lombok.Builder;
import lombok.Data;

/**
 * OrdersResponse
 */

 @Data
 @Builder
public class UserResponse {

    private String username;
    private String password;
    private String  name;
    private String  surname;
    private Date  date_of_birth;
    private List<Long>  books;
    
}