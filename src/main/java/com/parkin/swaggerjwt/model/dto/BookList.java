package com.parkin.swaggerjwt.model.dto;

import java.util.List;

import com.parkin.swaggerjwt.model.Book;

import lombok.Data;

@Data
public class BookList {

    private List<Book> books;

    public BookList(List<Book> books)
    {
        this.books = books;
    }


}