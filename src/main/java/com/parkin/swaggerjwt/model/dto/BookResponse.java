package com.parkin.swaggerjwt.model.dto;

import java.util.List;

import com.parkin.swaggerjwt.model.Book;

import lombok.Builder;
import lombok.Data;

/**
 * OrdersResponse
 */

 @Data
 @Builder
public class BookResponse {

    private List<Book> books;
    
}