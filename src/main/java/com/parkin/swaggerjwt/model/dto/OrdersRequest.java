package com.parkin.swaggerjwt.model.dto;

import lombok.Data;
import java.util.List;

import javax.validation.constraints.NotBlank;

@Data
public class OrdersRequest {

    @NotBlank
    private List<Long> orders;
    
}
