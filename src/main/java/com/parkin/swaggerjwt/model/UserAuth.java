package com.parkin.swaggerjwt.model;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class UserAuth {

  @Id
  public String id;

  public String userName;
  public String password;


  public UserAuth() {}

  public UserAuth(String userName, String password) {
    this.userName = userName;
    this.password = password;
  }

  @Override
  public String toString() {
    return String.format(
        "UserAuth[id=%s, userName='%s', password='%s']",
        id, userName, password);
  }

}