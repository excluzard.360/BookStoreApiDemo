package com.parkin.swaggerjwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import static java.lang.String.format;

import com.parkin.swaggerjwt.model.User;
import com.parkin.swaggerjwt.model.dto.UserRequest;
import com.parkin.swaggerjwt.services.UserService;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    UserService userService; 

    // UserRequest u = new UserRequest("","");
    UserRequest u = new UserRequest();

    public UserServiceTest() {
        this.u.setDate_of_birth(new Date());
        this.u.setUsername("TestUserName2");
        this.u.setPassword("Pass");
        this.u.setName("Name");
        this.u.setSurname("Surname");
    }

    @Test
    public void create() {

        this.userService.create(this.u);

        try {
            
            UserDetails u =  userService.loadUserByUsername(this.u.getUsername());
            Assert.assertEquals(this.u.getUsername(), u.getUsername());
        } catch (Exception e) {
            //TODO: handle exception
            System.out.println();
        }


    }

    @Test
    public void rmDelete() throws Exception {

        this.userService.delete(this.u.getUsername());
        try {
            UserDetails uDetail =  this.userService.loadUserByUsername(this.u.getUsername());
        } catch (UsernameNotFoundException e) {
            Assert.assertEquals(format("User with username - %s, not found", this.u.getUsername()), e.getLocalizedMessage());
        }

    }

}